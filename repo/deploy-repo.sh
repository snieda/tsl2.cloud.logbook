#!/bin/bash
# deploys all maven jars to your projects local repository 'repo'
#
# another way would be to use the maven-install-plugin. this could be started manually or in mvn on phase 'initialize' with goal 'install-file'
# example:
#    <configuration>
#        <groupId>my.local.jar</groupId> 
#        <artifactId>my-api</artifactId>
#        <version>1.0</version>
#        <packaging>jar</packaging>
#        <file>${basedir}/lib/my-api.jar</file>
#    </configuration>

rm -r net

WHERE=${1:-~/.m2/repository/net/sf/tsl2nano/tsl2*}
VERSION=${2:-2.5.1-SNAPSHOT}

shopt -s globstar
cp $WHERE/**/$VERSION/**/tsl*$VERSION.jar .
cp $WHERE/**/$VERSION/**/tsl*$VERSION.pom .

for f in $WHERE ; do mvn deploy:deploy-file -Durl=file://$PWD -Dfile=$(basename $f)-$VERSION.jar -DpomFile=$(basename $f)-$VERSION.pom -DgroupId=net.sf.tsl2nano -DartifactId=$(basename $f) -Dpackaging=jar -Dversion=$VERSION -DrepositoryId=project.local -DupdateReleaseInfo=true ; done;

# do it for one module as pom
#f=$(ls -1 $WHERE"h5-package")
f=~/.m2/repository/net/sf/tsl2nano/tsl2.nano.h5-package
mvn deploy:deploy-file -Durl=file://$PWD -Dfile=$(basename $f)-$VERSION.pom -DgroupId=net.sf.tsl2nano -DartifactId=$(basename $f) -Dpackaging=pom -Dversion=$VERSION -DrepositoryId=project.local -DupdateReleaseInfo=true

f=~/.m2/repository/net/sf/tsl2nano/tsl2.nano.h5.thymeleaf-pack
mvn deploy:deploy-file -Durl=file://$PWD -Dfile=$(basename $f)-$VERSION.pom -DgroupId=net.sf.tsl2nano -DartifactId=$(basename $f) -Dpackaging=pom -Dversion=$VERSION -DrepositoryId=project.local -DupdateReleaseInfo=true

rm *.jar *.pom
